---
title: "Reference"
linkTitle: "Reference"
weight: 9
date: 2020-03-17
description: >
  Low level reference docs for DPsim.
---

The [Sphinx documentation](https://acs.pages.rwth-aachen.de/public/simulation/dpsim/dpsim/about.html) has examples, build / installation instructions and covers the Python API.

The [Doxygen documentation](https://acs.pages.rwth-aachen.de/public/simulation/dpsim/dpsim/cxx/index.html) only includes automatically generated content using Doxygen. 
It is helpful to understand the general structure of the C++ DPsim core components.